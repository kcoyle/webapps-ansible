<include>
  <gateway name="sipgate.co.uk">
    <param name="proxy" value="sipgate.co.uk"/>
    <param name="username" value="{{ sipgate.username }}"/>
    <param name="password" value="{{ sipgate.password }}"/>
    <param name="extension" value="{{ sipgate.extension }}"/>
    <param name="register" value="true"/>
    <param name="context" value="public"/>
  </gateway>
</include>
