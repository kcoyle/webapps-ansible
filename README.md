### Vagrant Ansible Role

### Getting Started

```
# Install ansible galaxy dependencies
$ ansible-indico ansible-galaxy role install -r requirements.yml
# Boot Vagrant Box and Configure
# vagrant up
```

### Run

```bash
# ereo-scale
ansible-playbook playbooks/bbb-scale-server.yml --inventory inventory/production/hosts -e @host_vars/ereo-scale.yml --diff

# Server1
ansible-playbook playbooks/bbb-server.yml --inventory inventory/production/hosts -e @host_vars/ereo-server1.yml --diff
```